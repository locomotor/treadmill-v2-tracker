#include "MPU9250.h"
#include "Kalman.h"
#include "NeoPixelBrightnessBus.h"
#include <ESP8266WiFi.h>

#define NUMPIXELS 1
#define PIN_LED 3
#define PIN_SPI 5

const int INDEKS_IMUSA = 3; //0 - LU; 1 - LB; 2 - RU; 3 - RB; 4 - test;
const char* server = "192.168.2.118";
const char* ssid = "DiscoVR NewOffice 2.4G";
const char* password = "Garazowa4#";
const int port = 13000;

MPU9250 imu(SPI, PIN_SPI);
NeoPixelBrightnessBus<NeoRgbFeature, Neo800KbpsMethod> pixels(NUMPIXELS, PIN_LED);

uint32_t lastUpdate = 0, Now = 0;
float pitch, yaw, roll;
float deltat = 0.0f;

float q[4] = {1.0f, 0.0f, 0.0f, 0.0f};
float q1, q2, q3, q4;

RgbColor white(255, 255, 255);
RgbColor red(0, 0, 255);
RgbColor green(255, 0, 0);
RgbColor yellow(255, 255, 0);
RgbColor blue(0, 0, 255);

char letters[5]= {'A', 'B', 'C', 'D', 'T'};

WiFiClient client;
char buffer [10];

Kalman kalmanX;
Kalman kalmanY;

void connect()
{
  pixels.SetPixelColor(0, red);    pixels.Show();
  WiFi.begin(ssid, password);  
  while(WiFi.status()!=WL_CONNECTED)
    delay(100);
  pixels.SetPixelColor(0,blue);    pixels.Show();

  client.connect(server, port);
  while(!client.connected())
  {
    delay(100);        
    client.connect(server, port);
  }
  pixels.SetPixelColor(0,green);    pixels.Show();
}

void setup()
{
  pixels.Begin();

  pixels.Show();
  pixels.SetBrightness(10);

  pixels.SetPixelColor(0, white);    pixels.Show();
  delay(1000);
    
  while (imu.begin() < 0)
    delay(100);
  imu.setSrd(9);
  
  WiFi.mode(WIFI_STA);
  
  pixels.SetPixelColor(0,yellow);    pixels.Show();
  
  imu.calibrateGyro();
  imu.calibrateAccel();
  imu.calibrateMag();
  
  connect(); 

  KalmanSetup(imu.getAccelX_mss(), imu.getAccelY_mss(), imu.getAccelZ_mss());  
}

double kalAngleX, kalAngleY;
char res[100];
void takeCareOfEulers()
{
  imu.readSensor();
  int iter = 0;  
  float a[3];
  float g[3];
  float m[3];
  
  res[iter++] = letters[INDEKS_IMUSA];
  for(int stage = 0; stage < 9; stage++)
  {
    float val = 0;
    switch(stage)
    {
      case 0:        val = imu.getAccelX_mss(); a[0] = val; break;
      case 1:        val = imu.getAccelY_mss(); a[1] = val; break;
      case 2:        val = imu.getAccelZ_mss(); a[2] = val; break;
      case 3:        val = imu.getGyroX_rads(); g[0] = val; break;
      case 4:        val = imu.getGyroY_rads(); g[1] = val; break;
      case 5:        val = imu.getGyroZ_rads(); g[2] = val; break;
      case 6:        val = imu.getMagX_uT();    m[0] = val; break;
      case 7:        val = imu.getMagY_uT();    m[1] = val; break;
      case 8:        val = imu.getMagZ_uT();    m[2] = val; break;  
    }   
    
    char conv[8];
    sprintf(conv, "%f", val);
    int left = 8;
    while(left > 0)
      res[iter++] = conv[8-left--];
    if(stage<8) res[iter++] =  '|';
  }
  
  //Now = micros();
  //deltat = ((Now - lastUpdate)/1000000.0f);
  //lastUpdate = Now;
  
  //MadgwickQuaternionUpdate(a[0], a[1], a[2], g[0], g[1], g[2], m[0], m[1], m[2]);

  //KalmanUpdate(a[0], a[1], a[2], g[0], g[1], g[2]);  

  //res[iter++] = letters[INDEKS_IMUSA];
}

void loop()
{    
  if(client.connected())
  {      
    buffer[0] = 0;
    takeCareOfEulers();
    client.print(res);
  }
  else
  {
    connect();
  }
}
