double gyroXangle, gyroYangle, compAngleX, compAngleY;

void KalmanSetup(double ax, double ay, double az)
{
  double roll  = atan2(ay, az) * RAD_TO_DEG;
  double pitch = atan(-ax / sqrt(ay * ay + az * az)) * RAD_TO_DEG;

  kalmanX.setAngle(roll);
  kalmanY.setAngle(pitch);
  gyroXangle = roll;
  gyroYangle = pitch;
  compAngleX = roll;
  compAngleY = pitch;
}

void KalmanUpdate(double ax, double ay, double az, double gx, double gy, double gz)
{
  double roll = atan2(ay, az) * RAD_TO_DEG;
  double pitch = atan(-ax/sqrt(ay*ay+az*az))*RAD_TO_DEG;

  double gyroXrate = gx / 131.0;
  double gyroYrate = gy / 131.0;

  if((roll < -90 && kalAngleX > 90) ||(roll > 90 && kalAngleX< -90))
  {
    kalmanX.setAngle(roll);
    compAngleX = roll;
    kalAngleX = roll;
    gyroXangle = roll;
  }
  else
  {
    kalAngleX = kalmanX.getAngle(roll, gyroXrate, deltat);
  }

  if(abs(kalAngleX) > 90)
  {
    gyroYrate = -gyroYrate;
  }

  kalAngleY= kalmanY.getAngle(pitch, gyroYrate, deltat);

  gyroXangle += gyroXrate * deltat;
  gyroYangle += gyroYrate * deltat;

  compAngleX = 0.93 * (compAngleX + gyroXrate * deltat) + 0.07 * roll;
  compAngleY = 0.93 * (compAngleY + gyroYrate * deltat) + 0.07 * pitch;

  if(gyroXangle <-180 || gyroXangle > 180)
  {
    gyroXangle = kalAngleX;
  }
  if (gyroYangle < -180 || gyroYangle > 180)
  {
    gyroYangle = kalAngleY;    
  }
}
